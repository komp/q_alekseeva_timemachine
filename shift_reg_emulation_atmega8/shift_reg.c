#define F_CPU 1000000L
#include <avr/io.h>
#include <util/delay.h>


#define F_CPU 1000000L
#define IndicatorsLine1_DDR DDRD	
#define IndicatorsLine1_PORT PORTD
#define IndicatorsLine2_DDR DDRB
#define IndicatorsLine2_PORT PORTB
#define Service_DDR	DDRC
#define Service_PORT PORTC
#define Service_PIN PINC
#define DataPin 0
#define ClkPin 1
#define SignalLed 2


int CurrentNumber=0;
unsigned int buffer=0;
const unsigned int Marker_byte=0b11001010;

void Blinker(int number)
{
	for(int i=0; i<number; i++)
	{
		
			Service_PORT|=1<<SignalLed;
			_delay_ms(100);
				Service_PORT&=~1<<SignalLed;
			_delay_ms(100);
		
	}
	_delay_ms(2000);
}
int main(void)
{
	//������������� ������
	IndicatorsLine1_DDR=0xFF;	//�����
	IndicatorsLine2_DDR=0xFF;	//�����
	Service_DDR=1<<SignalLed;	//��� �� ����, ����� LED

	IndicatorsLine1_PORT=0x00;	
	IndicatorsLine2_PORT=0x00;	//���� ������� ���
	
	Service_PORT=0x00;		//������������� ��������� �� ����������, ������ �� led==0
	
	
	Service_PORT&=~1<<SignalLed;
	while(1)
	{
		buffer<<=1;
		while ((Service_PIN&(1<<ClkPin))==0){} //���� ��������� �������
		
		if ((Service_PIN&(1<<DataPin))>0)
		{
			buffer|=1;
		}
		
		if ((buffer>>8)==(Marker_byte))	//��������� ����� ������, ��������� 8 ����� �������� ��������
		{
			buffer&=0xFF;
			//��������
			if (buffer==CurrentNumber) //������!!
			{
				Service_PORT=1<<SignalLed;
			}
			CurrentNumber=buffer;
			if ((buffer<0)||(buffer>16))	//�����
			{
				IndicatorsLine1_PORT=0x00;	
				IndicatorsLine2_PORT=0x00;	
			}
			else
			{

				if(buffer>8)
				{
					buffer-=9;
					IndicatorsLine2_PORT=1<<buffer;
				}
				else
				{
					buffer--;
					IndicatorsLine1_PORT=1<<buffer;
				}
			}
		}
		while ((Service_PIN&(1<<ClkPin))>0){} //���� ��������� ����� ��������� �������
	}
}
