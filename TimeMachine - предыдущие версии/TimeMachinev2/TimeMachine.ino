#include <MsTimer2.h>
#include <iarduino_RTC.h>

//перечисленные ниже 4 параметра можно настраивать. Остальные не трогать!!!
int dest_day=22;    //день даты назначения
int dest_month=5;   //месяц даты назначения
int dest_year=1943;  //год даты назначения
int WaitTime=20;   //продолжительность временной задержки после перемотки, секунд
int DELTA_FPS=10000;       //чем больше данное значение, тем медленнее будет идти перемотка. Однако есть некоторый предел, который устанавливается экспериментальным путем
int DELAY_AFTER_TRIGGER=15; //задержка после путешествия в прошлое, минут

//ниже ничего не трогать!!!
#define ISR_PERIOD 2 //период прерываний для опроса энкодеров, мс
#define DELAY_INDICATORS 100//52
#define DELAY_CONNECTION_1 5 //3
#define DELAY_CONNECTION_2 6//10 
#define RESET_VAL 100
#define MARKER 0b11001010
#define NO_INDICATION 44


#define x1 12
#define x2 10
#define x3 9
#define x4 11


#define RTC_CLK 5
#define RTC_DAT 6
#define RTC_RST 7


#define DATA_PIN A1    
#define CLK_PIN A0
#define SignalLine 13 //signal-out-1

#define LeftLine A2
#define RightLine A3
#define LeftLine_2 A4
#define RightLine_2 A5
#define Switch 2 //btn1
#define Gerkon 3  //btn2
#define Blocking 8 //signal-in
#define Magnit 4  //signal-out-2

#define STAGE_MAIN 1
#define STAGE_WAIT_AFTER_PROCESSING 2

iarduino_RTC time(RTC_DS1302, RTC_RST, RTC_CLK, RTC_DAT); // подключаем RTC модуль на базе чипа DS1302, указывая выводы Arduino подключённые к выводам модуля RST, CLK, DAT

int k155[]={x1, x2, x3, x4};
byte cur_field=0; //[0..3] 0-стандартный режим, 1-настройка дня, 2 - настройка месяца, 3 - настройка года
int cur_year=0;   //текущая дата
int cur_month=0;
int cur_day=0;
int Date[4]; //Date(0) не используется

int present_day=0;
int present_month=0;
int present_year=0;

long delta=0;
int present_time=1;
int InTheOtherTime=0;
unsigned long int full_count=0;
int dir=0;
int pos=0;
int pos_2=0;
int flag_peremotki=0;
int is_stopped=0;

int tab=1; //планшет на месте

int numbers[16]; //массив цифр, каждая из которых соответствует одному индикатору
int numbers_backup_2[16];
int numbers_back[8];
int maximum[4];
int max_days_month[]={0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};


int fl_long_delay=1;    //определяем, нужно ли давать большую задержку

void ChangeNum(int);
void ChangeIndicator(int);

void InitNumbers(void);
void SetNumbers(int);
void Peremotka(void);
void timerInterupt(void);

//добавлено 20.12.2017
int fl_first_run=1;
int fl_activate_delay=0;
long int time_trigger=0;
int current_time_is_old=0;

//добавлено 26.12.17
int stages=STAGE_MAIN; //автомат состояний
long int tmp_mil=0;
long int timecounter=0;
//------------------

void setup() 
{
  delay(500);
  //EEPROM.write(ADDR_WAIT, 10&0xFF );
  //EEPROM.write(ADDR_WAIT+1, 0);
  time.begin();
  Serial.begin(9600);
  while (!Serial) { }// wait for serial port to connect. Needed for native USB port only

  pinMode(x1, OUTPUT);  //выводы для кодирования числа на индикаторе
  pinMode(x2, OUTPUT);  //выводы для кодирования числа на индикаторе
  pinMode(x3, OUTPUT); //выводы для кодирования числа на индикаторе
  pinMode(x4, OUTPUT); //выводы для кодирования числа на индикаторе

  digitalWrite(x1, LOW);
  digitalWrite(x2, LOW);
  digitalWrite(x3, LOW);
  digitalWrite(x4, LOW);

  pinMode(DATA_PIN, OUTPUT);
  pinMode(CLK_PIN, OUTPUT);
  pinMode(SignalLine, OUTPUT);
  pinMode(Magnit, OUTPUT);
  

  digitalWrite(DATA_PIN, LOW);
  digitalWrite(CLK_PIN, LOW);
  digitalWrite(SignalLine, HIGH);
  digitalWrite(Magnit, LOW);
  
  pinMode(LeftLine, INPUT);
  pinMode(RightLine, INPUT);
  pinMode(LeftLine_2, INPUT);
  pinMode(RightLine_2, INPUT);
  pinMode(Gerkon, INPUT);
  pinMode(Blocking, INPUT);
  pinMode(Switch, INPUT);

  digitalWrite(Switch, HIGH);
  digitalWrite(Blocking, HIGH);
  digitalWrite(Gerkon, HIGH);
  
  
  present_year=Date[3]=cur_year=atoi(time.gettime("Y"));
  present_month=Date[2]=cur_month=atoi(time.gettime("m"));
  present_day=Date[1]=cur_day=atoi(time.gettime("d"));
  
  maximum[0]=10;
  maximum[1]=31;
  maximum[2]=12;
  maximum[3]=3000;

  InitNumbers();

  MsTimer2::set(ISR_PERIOD, timerInterupt);  //прерывание с периодом ISR_PERIOD
  MsTimer2::start();

  //Serial.println("Prorgram started");
 // WaitTime=EEPROM.read(ADDR_WAIT);
  //WaitTime=EEPROM.read(ADDR_WAIT)&(EEPROM.read(ADDR_WAIT+1))<<8;    //2 байта для хранения продолжительности временной задержки перед открыванием магнита
  //Serial.println(WaitTime);
  
  while(1)
  {
	switch(stages)
	{
		case STAGE_MAIN:
			//основная работа - отображение чисел. значения передаются глобально. Все функции управления реализуются в прерывании, которое возникает через определенные промежутки времени
			
			if(fl_activate_delay)
			{
				stages=STAGE_WAIT_AFTER_PROCESSING;
				continue;
			}
			for (int i=0; i<16; i++)
			{
				if(numbers[i]==NO_INDICATION)
				{
					//в определенных случаях пропускаем некоторые индикаторы. Вводим доп. задержку, чтобы не пожечь остальные
					ChangeNum(numbers[i]); //выбрали цифру
					ChangeIndicator(NO_INDICATION);  //выбрали индикатор
					delayMicroseconds(DELAY_INDICATORS);
					//delay(5);
					ChangeIndicator(NO_INDICATION);  //сброс индикации, отображения нет
					continue;
				}
				ChangeNum(numbers[i]); //выбрали цифру
				ChangeIndicator(i+1);  //выбрали индикатор
				delayMicroseconds(DELAY_INDICATORS);
				//delay(5 );
				ChangeIndicator(NO_INDICATION);  //сброс индикации, отображения нет

				/*for( int i=0; i<16; i++)
				{
				Serial.print(numbers[i]);
				}
				Serial.println(""); */

			}
			break;
		case STAGE_WAIT_AFTER_PROCESSING:
			for (int i=0; i<16; i++)
			{
				ChangeNum(numbers[i]); //выбрали цифру
				ChangeIndicator(NO_INDICATION);  //выбрали индикатор
				delayMicroseconds(DELAY_INDICATORS);
				//delay(5);
				ChangeIndicator(NO_INDICATION);  //сброс индикации, отображения нет
			}
			if (!current_time_is_old)
			{
				fl_activate_delay=0;
				stages=STAGE_MAIN;
			}
				
		  if((millis()%1000==0)&&(millis()!=tmp_mil))
		  {
        tmp_mil=millis();
        if(++timecounter>=(DELAY_AFTER_TRIGGER*60))
        {
  				//время вышло
  				fl_activate_delay=0;
  				fl_first_run=1;
          timecounter=0;
  				stages=STAGE_MAIN;
        }
		  }
			break;
		default:
			break;
	} 
  }
}

void loop()
{
  while(1)
  {
  }
}

void ChangeNum(int val) //заданное число переводим в двоичный код и подаем соответствующие сигналы с помощью микросхемы управления
{
  for (int i=0; i<4; i++)
  {
    if(val%2==1)
    {
      digitalWrite(k155[i], HIGH);
      // Serial.print("1");
    }
    else 
    {
      digitalWrite(k155[i], LOW);
    //  Serial.print("0");
    }
    val=val>>1;
  }
}


void ChangeIndicator(int num)
{
  //отправить 1 байт-маркер, затем 1 байт полезной нагрузки
  unsigned int tmp=(MARKER<<8)|num;
  for( int i=0; i<16; i++)
  {
     if (tmp&(1<<15))
    {
      digitalWrite(DATA_PIN, HIGH);
    }
    else
    {
      digitalWrite(DATA_PIN, LOW);
    }
    tmp=tmp<<1;
    delayMicroseconds(DELAY_CONNECTION_1);
    //delay(10);
    digitalWrite(CLK_PIN, HIGH);
    delayMicroseconds(DELAY_CONNECTION_2);
    //delay(10);
    digitalWrite(CLK_PIN, LOW);
  } 
}

void InitNumbers(void)
{
  int tmp=cur_year;
  for(int i=7; i>3; i--)
  {
    numbers[i]=tmp%10;
    numbers[i+8]=tmp%10;
    tmp=tmp/10;
  }
  
  
  tmp=cur_day;
  numbers[1]=numbers[9]=tmp%10;
  tmp/=10;
  numbers[0]=numbers[8]=tmp%10;
  
  tmp=cur_month;
  numbers[3]=numbers[11]=tmp%10;
  tmp/=10;
  numbers[2]=numbers[10]=tmp%10;
  
}

void SetNumbers(int n)
{
  if(n==1) //меняем день назнаения
  {
    int tmp=Date[1];
    numbers[1]=tmp%10;
    tmp/=10;
    numbers[0]=tmp%10;
  }
  else if(n==2) //меняем месяц назначения
  {
    int tmp=Date[2];
    numbers[3]=tmp%10;
    tmp/=10;
    numbers[2]=tmp%10;
  }
  else if(n==3) //меняем год назначения
  {
    int tmp=Date[3];
    for(int i=7; i>3; i--)
    {
      numbers[i]=tmp%10;
      tmp=tmp/10;
    }
  }
  if(n==4) //меняем день текущий
  {
    int tmp=cur_day;
    numbers[9]=tmp%10;
    tmp/=10;
    numbers[8]=tmp%10;
  }
  else if(n==5) //меняем месяц текущий
  {
    int tmp=cur_month;
    numbers[11]=tmp%10;
    tmp/=10;
    numbers[10]=tmp%10;
  }
  else if(n==6) //меняем год текущий
  {
    int tmp=cur_year;
    for(int i=15; i>11; i--)
    {
      numbers[i]=tmp%10;
      tmp=tmp/10;
    }
  }
}

void Peremotka(void)
{
  ////Serial.println("Peremotka");
  ////Serial.println(full_count);
  ////Serial.println(dir);
  ////Serial.println(delta);
  if (dir==1) //мотаем назад
  {
    if (full_count<60)
    {
      delta=1;
    }
    if(cur_day>delta)
    {
      cur_day-=delta;
      SetNumbers(4);
    }
    else
    {
      if (cur_month==1)
      {
        cur_year--;
        cur_month=12;
        SetNumbers(5);
        SetNumbers(6);
      }
      else
      {
        cur_month--;
        SetNumbers(5);
      }
      cur_day=cur_day+max_days_month[cur_month]-delta;
      SetNumbers(4);
    }
    full_count-=delta;
    if ((full_count==0) || (cur_day==Date[1] && cur_month==Date[2] && cur_year==Date[3])) //пришли к требуемой дате
    {
      if (cur_year<Date[3])  //упс, промахнулись. меняем направление перемотки
      {
        dir=2;
      }
 
      if((cur_day!=Date[1]) || (cur_month!=Date[2]) || (cur_year!=Date[3]))
      {
        full_count+=delta;
        return;
      }
      full_count=0;
      flag_peremotki=0;
      /*for(int i=0; i<8; i++)    //убираем первую строчку. Станет доступна после того, как планшет снова вернут.
      {
         numbers[i]=NO_INDICATION;
      }*/
      //заказчику фича не понравилась, убираем
      //Serial.println("Peremotka stopped");
      InTheOtherTime=1; //освобождаем планшет и ждем его установки обратно
    }
  }
  else if(dir==2) //возвращаемся в будущее
  {
    if (full_count<30)
    {
      delta=1;
    }
    if(cur_day+delta<30)
    {
      cur_day+=delta;
      SetNumbers(4);
    }
    else
    {
      if (cur_month==12)
      {
        cur_year++;
        cur_month=1;
        SetNumbers(5);
        SetNumbers(6);
      }
      else
      {
        cur_month++;
        SetNumbers(5);
      }
      cur_day=cur_day-max_days_month[cur_month]+delta;
      SetNumbers(4);
    }
    full_count-=delta;
    if ((full_count==0)||(cur_day==Date[1] && cur_month==Date[2] && cur_year==Date[3])) //пришли к требуемой дате
    {
      if (cur_year>Date[3])  //упс, промахнулись. меняем направление перемотки
      {
        dir=1;
      }
      if((cur_day!=Date[1]) || (cur_month!=Date[2]) || (cur_year!=Date[3]))
      {
        full_count+=delta;
        return;
      }
      full_count=0;
      flag_peremotki=0;
     /* for(int i=0; i<8; i++)    //убираем первую строчку. Станет доступна после того, как планшет снова вернут.
      {
         numbers[i]=NO_INDICATION;
      }*/
      //заказчику фича не понравилась, убираем
      //Serial.println("Peremotka stopped");
      
      InTheOtherTime=1; //освобождаем планшет и ждем его установки обратно
      
    }
  }
}



void timerInterupt()
{     //управление устройством через данное прерывание
  if ((digitalRead(Gerkon) == HIGH)|| (digitalRead(Blocking) == LOW || (stages==STAGE_WAIT_AFTER_PROCESSING))) //планшет не на месте или машина заблокирована, отключаем машину
  { 
    for(int i=0; i<16; i++)
    {
       numbers[i]=NO_INDICATION;
    }
    //Serial.println("There is no battery.");
    tab=0;
    return;
  }
  else if ((digitalRead(Gerkon) == LOW) && !tab &&(digitalRead(Blocking) == HIGH) ) //планшет вернули
  {
    static int counter;
    counter++;
    if (counter<2000)   //костыль, чтобы избежать влияния дребезга контактов
    {
      return;
    }
    
    digitalWrite(SignalLine, HIGH);   //сигнализируем о штатной работе машины
    digitalWrite(Magnit, LOW);  //включаем магнит, планшет фиксируется
    tab=1;
    is_stopped=0; //снимаем блокировку
    //Serial.println("Tab on the position");
    InitNumbers();
  }

  
  if(InTheOtherTime)      //перемотка завершена, ждем заданный промежуток времени
  {
    static unsigned long tm;
    if (is_stopped==0)
    {
      ////Serial.println("is_stopped");
      tm = millis();
      is_stopped=1;     //запрещаем остальные операции
    }
    else if ((millis()-tm)>WaitTime*1000)  //задержка для видео и др эффектов
    {
      tm=0;
      InTheOtherTime=0;
      digitalWrite(Magnit, HIGH);  //отпускаем магнит, тем самым освобождается планшет
      fl_activate_delay=1;//задержка 15 минут
      current_time_is_old=~current_time_is_old;
      //Serial.println("Time mashine is stopped. Time is out.");
    }
    return;
  }

  
  if(is_stopped)  //машина остановлена, не требуется слушать ни кнопки, ни энкодеры.
  {
    return;
  }
  ////Serial.println("not stopped");
  if (flag_peremotki==1)
  {
    Peremotka();
    return;
  }
  if ((digitalRead(Switch) == LOW) && !cur_field) //нажата кнопка запуска и не редактируются никакие поля
  {
    ////Serial.println("Switch");
    
    if ((Date[3]!=cur_year)&&(((present_time==1)&&(Date[1]==dest_day)&&(Date[2]==dest_month)&&(Date[3]==dest_year))||((present_time!=1)&&(Date[1]==present_day)&&(Date[2]==present_month)&&(Date[3]==present_year)))) //если нужно возвратиться назад, то проверяем, верно ли выставлено время
    {
      //защита от дурака. Жёстко задаем дату назначения в соответствии со сценарием. Если дата выставлена неверно, сработки не произойдет.
      //в пределах одного года перемещаться запрещено
      //из прошлого можно вернуться только в определённую дату
      flag_peremotki=1;
      //cur_field=0;
      digitalWrite(SignalLine, LOW);
      //начинаем перемотку
      //время перемотки должно составить порядкка 20 секунд
      if(Date[3]<cur_year)  //всё хорошо, отправляемся в прошлое. Перемотка назад
      {
        dir=1;
        //для случая путешествия в прошлое
        int num_days_1=(12-Date[2]+1)*30-Date[1]; //количество дней до конца года
        ////Serial.println(num_days_1);
        
        int num_days_2=cur_month*30+cur_day;  //количество дней с начала года
        ////Serial.println(num_days_2);
        full_count=num_days_1+num_days_2+(cur_year-Date[3]-1)*360;  //для случая путешествия в прошлое
        ////Serial.println(full_count);
 
        float tmp_fl=full_count/(DELTA_FPS*1.0);
        delta=round(tmp_fl);    //значение примерное
        if (delta==0) //при попытке переместиться недалеко возможен такой результат. Ставим костыль.
        {
          delta=1;
        }
        ////Serial.println(delta);
      }
      else  if (cur_year<Date[3])//Путешествие в будущее
      {
        dir=2;
       //для случая возвращения в будущее
        int num_days_1=(12-cur_month+1)*30-cur_day; //количество дней до конца года
        int num_days_2=Date[2]*30+Date[1];  //количество дней с начала года
        
        full_count=num_days_1+num_days_2+(Date[3]-cur_year-1)*360; //для случая возвращения в будущее
        float tmp_fl=full_count/(DELTA_FPS*1.0);
        delta=round(tmp_fl);    //значение примерное
        if (delta==0) //при попытке переместиться недалеко возможен такой результат. Ставим костыль.
        {
          delta=1;
        }
      }
      
      if(present_time==1)     //время настоящее, или прошлое?
      {
        present_time=0;
      }
      else 
      {
        present_time=1;
      }
    }
    return;
  }
  
 //-------------------
  //работа с энкодерами
  ////Serial.println("enc1");
  if((digitalRead(LeftLine_2) == HIGH) and (digitalRead(RightLine_2) == HIGH))
  {
    pos_2=0;//pos==0 нейтральная позиция, точка отсчета
  }
  else
  {
    ////Serial.println("n1");
    if (pos_2==0)
    {
      if(digitalRead(LeftLine_2) ==LOW) //начало поворота налево
      {
        pos_2=1;
      }
      else if(digitalRead(RightLine_2) ==LOW)//начало поворота направо
      {
        pos_2=4;
      }
    }
    else if (pos_2==1)
    {
      if (digitalRead(RightLine_2) ==LOW) //зафиксирован поворот
      {
        if(Date[1]>max_days_month[Date[2]]) //проверяем, что значение в поле день соответствует значению в поле месяц. При необходимости корректируем.
        {
          Date[1]=max_days_month[Date[2]];
          //SetNumbers(1);
        }
        for(int i=0; i<8; i++)
        {
          if(numbers[i]!=NO_INDICATION)
          {
            numbers_back[i]=numbers[i];
            numbers[i]=NO_INDICATION;
          }
        }
        if(cur_field==0)
        {
          numbers[0]=numbers_back[0];
          numbers[1]=numbers_back[1];
        }
        else if(cur_field==1)
        {
          numbers[2]=numbers_back[2];
          numbers[3]=numbers_back[3];
        }
        else if(cur_field==2)
        {
          for(int i=4; i<8; i++)
          {
            numbers[i]=numbers_back[i];
          }
        }
        else if(cur_field==3)
        {
          for(int i=0; i<8; i++)
          {
            numbers[i]=numbers_back[i];
          }
        }
        pos_2=5;
        
        cur_field++;
        
        if (cur_field==4)
        {
          cur_field=0;
        }
        //Serial.print("Cur_field:");
       // //Serial.println(cur_field);
      }
    }
    else if (pos_2==4)
    {
      if (digitalRead(LeftLine_2) ==LOW) //зафиксирован поворот
      {
        if(Date[1]>max_days_month[Date[2]]) //проверяем, что значение в поле день соответствует значению в поле месяц. При необходимости корректируем.
        {
          Date[1]=max_days_month[Date[2]];
          //SetNumbers(1);
        }
        for(int i=0; i<8; i++)
        {
          if(numbers[i]!=NO_INDICATION)
          {
            numbers_back[i]=numbers[i];
            numbers[i]=NO_INDICATION;
          }
        }
        if(cur_field==2)
        {
          numbers[0]=numbers_back[0];
          numbers[1]=numbers_back[1];
        }
        else if(cur_field==3)
        {
          numbers[2]=numbers_back[2];
          numbers[3]=numbers_back[3];
        }
        else if(cur_field==0)
        {
          for(int i=4; i<8; i++)
          {
            numbers[i]=numbers_back[i];
          }
        }
        else if(cur_field==1)
        {
          for(int i=0; i<8; i++)
          {
            numbers[i]=numbers_back[i];
          }
        }
        pos_2=5;
        cur_field--;
        if(cur_field==255)
        {
          cur_field=3;
        }
        //Serial.print("Cur_field:");
        ////Serial.println(cur_field);
      }
    }
    else if (pos_2==5)//ничего не делаем, ждем, когда будет нейтральное положение
    {}
  }
  //-------------------
  
  //-------------------
  //работа с энкодерами
  ////Serial.println("enc2");
  if ((digitalRead(LeftLine) == HIGH) and (digitalRead(RightLine) == HIGH))
  {
    pos=0;//pos==0 нейтральная позиция, точка отсчета
  }
  else
  {
    ////Serial.println("n2");
    if (pos==0)
    {
      if(digitalRead(LeftLine) ==LOW) //начало поворота налево
      {
        pos=1;
      }
      else if(digitalRead(RightLine) ==LOW)//начало поворота направо
      {
        pos=4;
      }
    }
    else if (pos==1)
    {
      if (digitalRead(RightLine) ==LOW) //зафиксирован поворот налево
      {
        maximum[1]=max_days_month[Date[2]];   //максимум дней зависит от месяца
        pos=5;
        if (Date[cur_field]>maximum[cur_field]-1)
        {
          Date[cur_field]=1;
        }
        else
        {
          Date[cur_field]++;
        }
        //количество дней в фкврале зависит от года. Проверяем, при необходимости корректируем.
        if(cur_field==3)//если редактировался год, то проверяем, високосный ли он теперь
        {
          if (Date[3]%400==0)
          {
            max_days_month[2]=29;
          }
          else if (Date[3]%100==0)
          {
            max_days_month[2]=28;
          }
          else if (Date[3]%4==0)
          {
            max_days_month[2]=29;
          }
          else
          {
            max_days_month[2]=28;
          }
        }
        if(Date[1]>max_days_month[Date[2]]) //проверяем, что значение в поле день соответствует значению в поле месяц. При необходимости корректируем.
        {
          Date[1]=max_days_month[Date[2]];
          numbers_back[0]=Date[1]/10;
          numbers_back[1]=Date[1]%10;
        }
        SetNumbers(cur_field);
      }
    }
    else if (pos==4)
    {
      if (digitalRead(LeftLine) ==LOW) //зафиксирован поворот направо
      {
        maximum[1]=max_days_month[Date[2]];   //максимум дней зависит от месяца
        pos=5;
        if(Date[cur_field]==1)
        {
          Date[cur_field]=maximum[cur_field];
        }
        else
        {
          Date[cur_field]--;
        }
        if(cur_field==3)  //если редактировался год, то проверяем, високосный ли он теперь
        {
          if (Date[3]%400==0)
          {
            max_days_month[2]=29;
          }
          else if (Date[3]%100==0)
          {
            max_days_month[2]=28;
          }
          else if (Date[3]%4==0)
          {
            max_days_month[2]=29;
          }
          else
          {
            max_days_month[2]=28;
          }
        }
        if(Date[1]>max_days_month[Date[2]]) //проверяем, что значение в поле день соответствует значению в поле месяц. При необходимости корректируем.
        {
          Date[1]=max_days_month[Date[2]];
          numbers_back[0]=Date[1]/10;
          numbers_back[1]=Date[1]%10;
          //SetNumbers(1);
        }
        SetNumbers(cur_field);
      }
    }
    else if (pos==5)//ничего не делаем, ждем, когда будет нейтральное положение
    {}
  }
  //-------------------
}

